package com.gitlab.et.paralleltests.helper

import com.gitlab.et.paralleltests.ParallelTestExtension

class DynamicConfigLoader {
    ParallelTestExtension extension = new ParallelTestExtension()

    DynamicConfigLoader(ParallelTestExtension extension) {
        this.extension = extension
    }

    ParallelTestExtension overrideByGivenParameters() {
        Hashtable<String, String> vars = EnvironmentVariableRetriever.getEnvVarsWithoutPrefix("testParallel")
        List<String> keysOfVars = Collections.list(vars.keys())

        if (keysOfVars.contains("printConfiguration")) {
            extension.printConfiguration = vars["printConfiguration"].toBoolean()
        }
        if (keysOfVars.contains("parallelExecutions")) {
            extension.parallelExecutions = vars["parallelExecutions"].toInteger()
        }
        if (keysOfVars.contains("cucumberCliClass")) {
            println vars["cucumberCliClass"]
            extension.cucumberCliClass = vars["cucumberCliClass"]
        }
        if (keysOfVars.contains("properties")) {
            extension.properties = vars["properties"]
        }
        if (keysOfVars.contains("glue")) {
            extension.glue = vars["glue"]
        }
        if (keysOfVars.contains("featureDir")) {
            extension.featureDir = vars["featureDir"]
        }
        if (keysOfVars.contains("tags")) {
            extension.tags = vars["tags"]
        }
        if (keysOfVars.contains("environmentVariablePrefix")) {
            extension.environmentVariablePrefix = vars["environmentVariablePrefix"]
        }
        if (keysOfVars.contains("strict")) {
            extension.strict = vars["strict"].toBoolean()
        }
        if (keysOfVars.contains("printTestOutput")) {
            extension.printTestOutput = vars["printTestOutput"].toBoolean()
        }
        if (keysOfVars.contains("parallelScenarios")) {
            extension.parallelScenarios = vars["parallelScenarios"].toBoolean()
        }
        if (keysOfVars.contains("shortenPathForLogging")) {
            extension.shortenPathForLogging = vars["shortenPathForLogging"].toBoolean()
        }
        if (keysOfVars.contains("cucumberPlugins")) {
            extension.cucumberPlugins = vars["cucumberPlugins"].split(";")
        }
        return extension
    }

}
