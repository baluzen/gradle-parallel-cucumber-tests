package com.gitlab.et.paralleltests.helper


import com.gitlab.et.paralleltests.helper.model.Test

import java.util.concurrent.TimeUnit

class FormattedPrinter {

    public static printTestResults(List<Test> tests, PrintStream stream = System.out) {
        Integer maximumLengthOfName = tests.name.collect { it.size() }.max()
        Integer maximumLengthOfResult = tests.result.collect { it.name().size() }.max()
        Integer maximumLengthOfExecutionTime = tests.executionTimeInMilliSeconds.collect {
            milliSecondsToMinSecMs(it).size()
        }.max()
        if (maximumLengthOfExecutionTime < 14)
            maximumLengthOfExecutionTime = 14

        String alignment = " | %-${maximumLengthOfName}s | %-${maximumLengthOfResult}s | %-${maximumLengthOfExecutionTime}s |\n";


        stream.format(alignment, "-" * maximumLengthOfName, "-" * maximumLengthOfResult, "-" * maximumLengthOfExecutionTime)
        stream.format(alignment, "Test", "Result", "Execution Time")
        stream.format(alignment, "-" * maximumLengthOfName, "-" * maximumLengthOfResult, "-" * maximumLengthOfExecutionTime)
        tests.each {
            stream.format(alignment, it.name, it.result, milliSecondsToMinSecMs(it.executionTimeInMilliSeconds))
        }


    }


    private static String milliSecondsToMinSecMs(long ms) {
        long min = TimeUnit.MILLISECONDS.toMinutes(ms)
        ms -= TimeUnit.MINUTES.toMillis(min)

        long sec = TimeUnit.MILLISECONDS.toSeconds(ms)
        ms -= TimeUnit.SECONDS.toMillis(sec)

        String str = ""
        if (min > 0)
            str += "$min min, $sec sec, "
        else if (sec > 0)
            str += "$sec sec, "
        str += "$ms ms"

        return str
    }

}
