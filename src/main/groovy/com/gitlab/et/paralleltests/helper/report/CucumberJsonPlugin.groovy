package com.gitlab.et.paralleltests.helper.report

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.apache.commons.lang3.RandomStringUtils

class CucumberJsonPlugin {
    List<String> testResultFileNames = new ArrayList<>()

    String generateAndAddRandomNameSuffix(String pluginParameter) {
        String testResultFileName = pluginParameter + randomNameSuffix()
        testResultFileNames << testResultFileName
        return testResultFileName

    }

    private static String randomNameSuffix() {
        int randomStringLength = 10
        String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
        String randomString = RandomStringUtils.random(randomStringLength, charset.toCharArray())
        String currentMillis = System.currentTimeMillis().toString()

        return "_" + currentMillis + "_" + randomString

    }

    public mergeJsonData(String plugin) {
        def jsonCucumbers = new ArrayList<>()

        testResultFileNames.each {
            def jsonCucumber = new JsonSlurper().parseText(new File(it.replaceFirst("json:", "")).text)
            if(jsonCucumber){
                jsonCucumbers << jsonCucumber[0]
            }
        }

        def json = JsonOutput.toJson(jsonCucumbers)
        new File(plugin.replaceFirst("json:", "")).write(json)
    }


}
