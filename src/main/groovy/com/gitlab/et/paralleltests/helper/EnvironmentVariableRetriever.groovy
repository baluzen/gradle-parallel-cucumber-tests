package com.gitlab.et.paralleltests.helper

class EnvironmentVariableRetriever {


    public static Hashtable<String, String> getEnvVars(String environmentVariablePrefix) {
        if (!environmentVariablePrefix.isEmpty()) {
            return System.properties.findAll { it.key.toString().startsWith(environmentVariablePrefix) }
        }

        return new Hashtable<String, String>()
    }

    public static Hashtable<String, String> getEnvVarsWithoutPrefix(String environmentVariablePrefix) {
        Hashtable<String, String> envsWithoutPrefix = new Hashtable<String, String>()

        getEnvVars(environmentVariablePrefix).each {
            String key = it.key.toString().replace("${environmentVariablePrefix}.", "")
            String value = it.value.toString()
            envsWithoutPrefix.put(key, value)
        }

        return envsWithoutPrefix
    }

}

