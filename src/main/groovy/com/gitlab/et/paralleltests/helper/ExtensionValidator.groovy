package com.gitlab.et.paralleltests.helper

import com.gitlab.et.paralleltests.ParallelTestExtension

class ExtensionValidator {

    ParallelTestExtension extension;


    ExtensionValidator(extension) {
        this.extension = extension
    }


    def checkMandatoryParametersArePresent() {
        if (!extension.cucumberCliClass) {
            throw new MissingPropertyException("Mandatory parameter \'cucumberCliClass\' is not set")
        }
        if (!extension.glue) {
            throw new MissingPropertyException("Mandatory parameter \'glue\' is not set")
        }
        if (!extension.featureDir) {
            throw new MissingPropertyException("Mandatory parameter \'featureDir\' is not set")
        }
        if (!extension.parallelExecutions || extension.parallelExecutions == 0) {
            throw new MissingPropertyException("Mandatory parameter \'parallelExecutions\' is not set")
        }
    }


    def printConfiguration() {
        if (extension.printConfiguration) {

            println "Configuration:"
            formattedPrint("Cucumber cli class", "$extension.cucumberCliClass")
            formattedPrint "Maximum parallel executions", "$extension.parallelExecutions"
            formattedPrint "Run scenarios in parallel", "$extension.parallelScenarios"
            formattedPrint "Path of feature files", "$extension.featureDir"
            formattedPrint "Path of glue code", "$extension.glue"
            formattedPrint "Cucumber plugins", extension.cucumberPlugins
            formattedPrint "Run following tags", "$extension.tags"
            formattedPrint "Fail on pending steps (strict)", "$extension.strict"
            formattedPrint "Additional properties", "$extension.properties"
            formattedPrint "Print test output", "$extension.printTestOutput"
            formattedPrint "Shorten path for logging ", "$extension.shortenPathForLogging"
            formattedPrint "Environmental variable prefix", "$extension.environmentVariablePrefix"
            if (!extension.environmentVariablePrefix.isEmpty()) {
                println "\t-found following environment variables:"
                def vars = EnvironmentVariableRetriever.getEnvVars(extension.environmentVariablePrefix)
                if (!vars.isEmpty()) {
                    vars.each { println "\t\t$it.key=$it.value" }
                }
            }
            println "\n"

        }
    }

    private formattedPrint(String var1, String var2) {
        String alignment = "%-30s : %s\n";
        System.out.format(alignment, var1, var2)

    }

    private formattedPrint(String var, List<String> arr) {
        formattedPrint(var, arr.size > 0 ? arr[0] : "")

        if (arr.size() > 1) {
            arr[1..arr.size() - 1].each {
                formattedPrint("", it)
            }
        }

    }

}
