Feature: Check Addition

  This scenarios checking that the addition works correctly


  Scenario: Add numbers to correct result 7
    Given x is set to 2
    And y is set to 4
    When the calculator adds x to y
    Then the result is 6
