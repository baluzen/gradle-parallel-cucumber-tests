Feature: Check Addition

  This scenarios checking that the addition works correctly

  @yes
  Scenario: Add numbers to correct result 1
    Given x is set to 8
    And y is set to 2
    When the calculator adds x to y
    Then the result is 10
