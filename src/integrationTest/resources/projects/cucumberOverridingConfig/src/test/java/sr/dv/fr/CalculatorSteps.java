package sr.dv.fr;



import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.assertj.core.api.Assertions.assertThat;


public class CalculatorSteps {

    private Calculator calculator = new Calculator();
    private int x = 0;
    private int y = 0;
    private int result = 0;

    @Given("x is set to (\\d+)")
    public void x_is_set_to(int x) {
        this.x = x;
    }

    @Given("y is set to (\\d+)")
    public void y_is_set_to(int y) {
        this.y = y;
    }

    @When("the calculator adds x to y")
    public void the_calculator_adds_x_to_y() {
        result = calculator.add(x, y);
    }

    @Then("the result is (\\d+)")
    public void the_result_is(int result) {
        assertThat(result).isEqualTo(this.result);
    }


}
