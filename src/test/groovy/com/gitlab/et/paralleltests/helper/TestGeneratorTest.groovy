package com.gitlab.et.paralleltests.helper

import com.gitlab.et.paralleltests.ParallelTestExtension
import com.gitlab.et.paralleltests.helper.model.Feature
import com.gitlab.et.paralleltests.helper.model.Scenario
import com.gitlab.et.paralleltests.helper.model.Test
import com.gitlab.et.paralleltests.helper.report.CucumberPluginResolver
import spock.lang.Specification

class TestGeneratorTest extends Specification {


    def "Generates one test for feature"() {
        given:
        ParallelTestExtension extension = [parallelExecutions: 1,
                                           cucumberCliClass  : 'custom.cucumber.api.cli.Main',
                                           properties        : '',
                                           //tags              : "",
                                           strict            : true,
                                           parallelScenarios : false,
                                           glue              : 'test/glue',
                                           featureDir        : 'test/features'
        ]
            TestGenerator testGenerator = new TestGenerator(extension, new CucumberPluginResolver())
        Test expectedTest = [name: "path/to/file1", command: ["java",
                                                              "custom.cucumber.api.cli.Main",
                                                              "--glue",
                                                              "test/glue",
                                                              "--strict",
                                                              "path/to/file1"]
        ]

        when:
        List<Test> actualTests = testGenerator.generateTestsForFeature(simpleFeature())

        then:
        actualTests.size() == 1
        actualTests.first() == expectedTest
    }



    def "Generates tests for all scenarios in feature"() {
        given:
        ParallelTestExtension extension = [parallelExecutions: 1,
                                           cucumberCliClass  : 'cucumber.api.cli.Main',
                                           properties        : '',
                                           //tags              : "",
                                           strict            : true,
                                           parallelScenarios : true,
                                           glue              : 'test/glue',
                                           featureDir        : 'test/features'
        ]
        TestGenerator testGenerator = new TestGenerator(extension, new CucumberPluginResolver())

        def name = simpleFeature().path
        List<Test> expectedTests = []
        3.times {
            expectedTests.add(new Test([name: "$name:f1_sc$it", command: ["java",
                                                                          "cucumber.api.cli.Main",
                                                                          "--glue",
                                                                          "test/glue",
                                                                          "--strict",
                                                                          name,
                                                                          "--name",
                                                                          "^f1_sc$it\$"]
            ]))
        }



        when:
        List<Test> actualTests = testGenerator.generateTestsForFeature(simpleFeature())

        then:
        expectedTests==actualTests
    }


    Feature simpleFeature() {
        Feature f = [path: "path/to/file1"]
        3.times { f.scenarios.add(new Scenario([name: "^f1_sc$it\$"])) }
        return f
    }

}
